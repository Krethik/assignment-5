*** Settings ***
Library  SeleniumLibrary

*** Variables ***
${browser}   chrome
${url}  https://demoqa.com/automation-practice-form

*** Test Cases ***
TestingInputBox
   open browser      ${url}     ${browser}
   maximize browser window
   sleep   5s
   input text        xpath://input[@id='firstName']    Krethik
   input text        xpath://input[@id='lastName']             B
   input text        xpath://input[@id='userEmail']          krethikmagesh@gmail.com
   click element     xpath://label[@for='gender-radio-1']
   input text        xpath://input[@id='userNumber']       8618504689
   click element     xpath://input[@class='form-control']
   click element    xpath://input[@id='dateOfBirthInput']
   Select From List By Label    //select[@class='react-datepicker__year-select']    2001
   Select From List By Label    //select[@class='react-datepicker__month-select']    October
   Click Element    //*[@aria-label='Choose Friday, October 26th, 2001']
   Click Element    xpath://input[@id='subjectsInput']
   Input Text    xpath://input[@id='subjectsInput']     PCM
   click element     xpath://label[@for='hobbies-checkbox-1']
   input text        xpath://textarea[@placeholder='Current Address']     SBIOA Unity Enclave
   sleep  5s



